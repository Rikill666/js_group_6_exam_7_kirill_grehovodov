import React from 'react';

const FullPrice = ({productsList}) => {
    let price = 0;
    productsList.forEach(function (product){
        price += (product.price * product.count );
    });
    return (
        <p className="h5">
            _________________________________<br/>
            Total price: {price} KGS
        </p>
    );
};

export default FullPrice;