import React from 'react';
import Product from "../Product/Product";
import nanoid from "nanoid";

const products = [
    {name: 'Hamburger', price: 80, id: nanoid()},
    {name: 'Coffee', price: 70, id: nanoid()},
    {name: 'Tea', price: 20, id: nanoid()},
    {name: 'Lemonade', price: 35, id: nanoid()},
    {name: 'IceCream', price: 50, id: nanoid()},
    {name: 'Cookie', price: 45, id: nanoid()}
];
const AddItems = ({addProduct}) => {
    return (
        <div className='col-12 col-md-8 col-lg-8 border border-dark'>
            <h5>Add Items: </h5>
            <div className="row">
                {products.map(product =>
                    <Product key={product.id} addProduct={addProduct} product={product}/>
                )}
            </div>
        </div>
    );
};

export default AddItems;