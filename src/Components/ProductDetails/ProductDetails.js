import React from 'react';
import './ProductDetails.css';

const ProductDetails = ({product, deleteClick}) => {
    return (
        <li>
            <p className='liChild'>{product.name}</p>
                <p className='liChild'>x{product.count} <span className='pl-5'>{product.price * product.count} KGS</span></p>
            <i onClick={deleteClick} className="fas fa-times del"/>
        </li>
    );
};

export default ProductDetails;