import React from 'react';
import ProductDetails from "../ProductDetails/ProductDetails";
import FullPrice from "../FullPrice/FullPrice";
import 'bootstrap/dist/css/bootstrap.css';

const OrderDetails = ({productsList, deleteProduct}) => {
    if (productsList.length === 0) {
        return (
            <div className='col-12 col-md-4 border border-dark'>
                <h5>Order Details: </h5>
                <p>Order is empty! <br/>Please add some items!</p>
            </div>
        );
    } else {
        return (
            <div className='col-12 col-md-4  border border-dark'>
                <h5>Order Details: </h5>
                <ul className='pl-0'>
                    {productsList.map(product =>
                        <ProductDetails deleteClick={() => deleteProduct(product.id)} key={product.id} product={product}/>
                    )}
                </ul>
                <FullPrice productsList={productsList}/>
            </div>
        );
    }

};

export default OrderDetails;