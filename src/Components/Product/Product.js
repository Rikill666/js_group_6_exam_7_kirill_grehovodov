import React from 'react';
import './Product.css'

const Product = ({product, addProduct}) => {
    let buttonIcon = "fas ";
    if (product.name === 'Hamburger') {
        buttonIcon += "fa-hamburger";
    } else if (product.name === 'Coffee') {
        buttonIcon += "fa-coffee";
    } else if (product.name === 'Tea') {
        buttonIcon += "fa-mug-hot";
    } else if (product.name === 'Lemonade') {
        buttonIcon += "fa-glass-whiskey";
    } else if (product.name === 'IceCream') {
        buttonIcon += "fa-ice-cream";
    } else {
        buttonIcon += "fa-cookie";
    }
    return (
        <div className='col-12 col-md-6 mb-2'>
            <button onClick={() => addProduct(product)} className='btn btn-outline-dark w-100'>
                <div className="row">
                    <div className="col-4">
                        <i className={buttonIcon}/>
                    </div>
                    <div className="col-8">
                        <h4>
                            {product.name}
                        </h4>
                        <p>
                            Price: {product.price} KGS
                        </p>
                    </div>
                </div>
            </button>
        </div>
    );
};

export default Product;