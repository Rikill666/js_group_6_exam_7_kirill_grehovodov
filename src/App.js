import React, {Component} from 'react';
import AddItems from "./Components/AddItems/AddItems";
import OrderDetails from "./Components/OrderDetails/OrderDetails";
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
    state = {
        products: []
    };
    addProduct = product => {
        const products = [...this.state.products];
        const index = this.state.products.findIndex(t => t.id === product.id);
        if (index === -1) {
            let p = {...product, count: 1};
            products.push(p);
        } else {
            products[index].count++;
        }
        this.setState({products});
    };
    deleteProduct = id => {
        const products = [...this.state.products];
        const index = this.state.products.findIndex(t => t.id === id);
        products.splice(index, 1);
        this.setState({products});
    };

    render() {
        return (
            <div className="container mt-3">
                <div className="row">
                    <OrderDetails deleteProduct={this.deleteProduct} productsList={this.state.products}/>
                    <AddItems addProduct={this.addProduct}/>
                </div>
            </div>
        );
    }
}

export default App;
